Source: tcpdf
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper (>= 11)
Rules-Requires-Root: binary-targets
Standards-Version: 4.1.3
Section: php
Homepage: http://www.tcpdf.org/
Vcs-Git: https://salsa.debian.org/debian/tcpdf.git
Vcs-Browser: https://salsa.debian.org/debian/tcpdf

Package: php-tcpdf
Architecture: all
Depends: ${misc:Depends}
Recommends: php-gd, php-mcrypt
Suggests: php-imagick
Description: PHP class for generating PDF files on-the-fly
 TCPDF is a library to generate PDF files that does not require external
 extensions. It also includes a class to extract data from existing PDF
 documents and classes to generate 1D and 2D barcodes in various formats.
 .
 TCPDF has been originally derived from the Public Domain FPDF class by
 Olivier Plathey (http://www.fpdf.org).
 .
 Its main features are:
 .
  * an extensive API to control the generated content and its layout
    (including using XHTML as input);
  * works without external libraries;
  * works with any language (UTF-8 and RTL support);
  * supports TrueTypeUnicode, TrueType, Type1 and CID-0 fonts;
  * supports all page formats (standardized and customized);
  * supports most PDF features:
    - encryption and digital signatures
    - compression
    - bookmarks
    - javascript and forms support
    - PDF annotations, including links, text and file attachments
    - XOBject Templates (layers and object visibility, PDF/A-1b support)
  * supports barcode generation (multiple formats);
  * automatic hyphenation and page break.
